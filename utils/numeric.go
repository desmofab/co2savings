package utils

import (
	"fmt"
	"strconv"
)

// Truncate number to 2 decimals
func ToFixed(number float64) float64 {
	strNumber := fmt.Sprintf("%.2f", number)
	floatNumber, _ := strconv.ParseFloat(strNumber, 64)
	return floatNumber
}