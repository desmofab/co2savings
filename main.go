package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	c "termo.com/co2savings/controllers"
	u "termo.com/co2savings/utils"
)

func setupRouter() *gin.Engine {
	router := gin.Default()
	router.Use(u.CORSMiddleware())
	router.Static("/assets", "./assets")
	router.LoadHTMLGlob("views/*")

	// Load Homepage
	router.GET("/", func(ginContext *gin.Context) {
		ginContext.HTML(http.StatusOK, "index.html", gin.H{
			"title": "CO2 Savings Calculator",
			"message": "Calculate your CO2 savings",
		})
	})

	// GET API CO2 savings calculation
	router.GET("/api/v1/co2savings", c.GetCO2Savings)

	// 404 Redirect
	router.NoRoute(func(ginContext *gin.Context) {
		ginContext.HTML(http.StatusOK, "404.html", gin.H{
			"title": "PAGE_NOT_FOUND",
			"message": "Page not found",
		})
	})

	return router
}

func main() {
	router := setupRouter()
	router.Run(":8080")
}
