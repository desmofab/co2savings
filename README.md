# CO2 Savings Calculator

This tool is based on [Gin Web Framwork](https://github.com/gin-gonic/gin) as beackend REST APIs manager and React for the UI.  
Actually, it's a complete web app, built on top of the MVC offered by Gin.

## **Tl;dr** How to test this app?

You can run a **Docker container**. To keep it simple, the app runs on `http://localhost:8080/api/v1/co2savings?installs=<number of installations>` for the API,
and `http://localhost:8080` for the UI homepage.  
In order to test the REST API you have to pass the *GET-string* parameter `installs` by using an integer (see Covered aspects section below).

---

## Backend REST API

**Endpoint**: `http://localhost:8080/api/v1/co2savings?installs=<number of installations>`

Covered aspects:

- [x] Input paramter `installs` sanitization and validation (it is mandatory, and must be a numeric value (integer) between 1 and 100).
- [x] Unit testing. Check out the ***tests*** folder.
- [x] Docker containerization.

CO2 Savings Calculation criteria:

To calculate the result of CO2 savings broken down by `year`, `month`, `week` and `day` the simple *arithmetic average* has been used instead of the *weighted average*.  
Actually, using the latter, the result would be exactly the same of the former, also using for example, a weight ratio of *2:1* for cold seasons.

## Frontend React UI

**URL**: `http://localhost:8080`

***Gin*** offer the *server side rendering* feature for HTML templates. ***React***, ***Babel*** and ***MUI*** are being included just as pure libraries.  
Moreover, ***React HOOKS*** have been used to manage the app state and functionalities.
To keep the UI simple, the limit of the *slider component* has been set to *10*.
