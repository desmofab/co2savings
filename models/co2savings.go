package models

type CO2Savings struct {
	Day                 float64 `json:"day"`
	Week                float64 `json:"week"`
	Month               float64 `json:"month"`
	Year                float64 `json:"year"`
	InstallationsNumber int     `json:"installationsNumber"`
	SavingCoefficient   int     `json:"savingCoefficient"`
}


type GetParams struct {
	InstallsNumber int `form:"installs" json:"installs" binding:"required,gte=1,lte=100"`
}

type HasBadParam bool

const (
	DEFAULT_SAVINGS_COEFFICIENT = 600
	DAYS_IN_YEAR = 365
	WEEKS_IN_YEAR = 52
	MONTHS_IN_YEAR = 12
)