package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	m "termo.com/co2savings/models"
	u "termo.com/co2savings/utils"
)


func GetCO2Savings(ginContext *gin.Context) {
	params, isBadRequest := checkParams(ginContext)

	if isBadRequest {
		return
	}

	co2Saved := calculateCO2Saved(params.InstallsNumber, m.DEFAULT_SAVINGS_COEFFICIENT)

	ginContext.JSON(200, co2Saved)
}

func checkParams(ginContext *gin.Context) (m.GetParams, m.HasBadParam) {
	var getParams m.GetParams
	var hasBadParam m.HasBadParam
	if err := ginContext.BindQuery(&getParams); err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{
			"error":   err.Error(),
			"message": "'installs' parameter is required and must be a numeric value (integer) between 1 and 100",
		})
		hasBadParam = true
	}
	return getParams, hasBadParam
}

func calculateCO2Saved(InstallsNumber, savingCoefficient int) m.CO2Savings {
	var co2Savings m.CO2Savings
	co2Savings.InstallationsNumber = InstallsNumber
	co2Savings.SavingCoefficient = savingCoefficient
	co2Savings.Year = float64(InstallsNumber * savingCoefficient)
	co2Savings.Month = u.ToFixed(co2Savings.Year / m.MONTHS_IN_YEAR)
	co2Savings.Week = u.ToFixed(co2Savings.Year / m.WEEKS_IN_YEAR)
	co2Savings.Day = u.ToFixed(co2Savings.Year / m.DAYS_IN_YEAR)
	return co2Savings
}