package tests

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	c "termo.com/co2savings/controllers"
	m "termo.com/co2savings/models"
)


func setupRouter() *gin.Engine {
	r := gin.Default()

	// GET CO2 savings calculation
	r.GET("/api/v1/co2savings", c.GetCO2Savings)

	return r
}

func TestValidResult(t *testing.T) {
	router := setupRouter()

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/co2savings?installs=3", nil)
	router.ServeHTTP(res, req)

	var co2Savings m.CO2Savings
	assert.Equal(t, 200, res.Code)
	json.Unmarshal(res.Body.Bytes(), &co2Savings)

	assert.Equal(t, float64(1800), co2Savings.Year)
	assert.Equal(t, float64(150), co2Savings.Month)
	assert.Equal(t, 34.62, co2Savings.Week)
	assert.Equal(t, 4.93, co2Savings.Day)
}

func TestMandatoryParam(t *testing.T) {
	router := setupRouter()

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/co2savings", nil)
	router.ServeHTTP(res, req)

	assert.Equal(t, 400, res.Code)
}

func TestNumericParam(t *testing.T) {
	router := setupRouter()

	respCodeByValue := map[int]int {
		-1: 400,
		0: 400,
		1: 200,
		100: 200,
		101: 400,
		99999: 400,
	}

	for value, code := range respCodeByValue {
		uri := fmt.Sprintf("%s%d", "/api/v1/co2savings?installs=", value)
		res := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", uri, nil)
		router.ServeHTTP(res, req)
	
		assert.Equal(t, code, res.Code)
	}
}

func TestAlphanumParam(t *testing.T) {
	router := setupRouter()

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/co2savings?installs=fsdf2345", nil)
	router.ServeHTTP(res, req)

	assert.Equal(t, 400, res.Code)
}