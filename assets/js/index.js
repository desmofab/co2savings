const BASE_API_URL = 'http://localhost:8080/api/v1'
const DEFAULT_INSTALLS = 3
const {
  colors,
  CssBaseline,
  ThemeProvider,
  Typography,
  Container,
  createTheme,
  Box,
  Grid,
  SvgIcon,
  Slider,
  Link,
  Divider,
} = MaterialUI;

// Create a theme instance.
const theme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: '#0575bc',
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: colors.red.A400,
    },
  },
});

const fetchSavings = (installs) => fetch(`${BASE_API_URL}/co2savings?installs=${installs}`)

function LightBulbIcon(props) {
  return (
    <SvgIcon {...props}>
      <path d="M9 21c0 .55.45 1 1 1h4c.55 0 1-.45 1-1v-1H9v1zm3-19C8.14 2 5 5.14 5 9c0 2.38 1.19 4.47 3 5.74V17c0 .55.45 1 1 1h6c.55 0 1-.45 1-1v-2.26c1.81-1.27 3-3.36 3-5.74 0-3.86-3.14-7-7-7zm2.85 11.1l-.85.6V16h-4v-2.3l-.85-.6C7.8 12.16 7 10.63 7 9c0-2.76 2.24-5 5-5s5 2.24 5 5c0 1.63-.8 3.16-2.15 4.1z" />
    </SvgIcon>
  );
}

function ProTip() {
  return (
    <Typography sx={{ mt: 6, mb: 3 }} color="text.secondary" align="center">
      <LightBulbIcon sx={{ mr: 1, verticalAlign: 'top' }} />
      Select the numeber of installations
    </Typography>
  );
}

function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://termostore.it/" target="_blank">
        Termo
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

function Savings(props) {
  if (Object.entries(props.co2savings).length === 0) {return null}
  const co2savings = {
    yearly: props.co2savings.year,
    monthly: props.co2savings.month,
    weekly: props.co2savings.week,
    daily: props.co2savings.day
  }
  return (
    <Box sx={{ flexGrow: 1, paddingBottom: 3}}>
      <Typography variant="h5" align="center" gutterBottom>
        CO2 Savings in Kg
      </Typography>
      <Grid container spacing={3} direction="row" justifyContent="center" alignItems="center" columns={4}>
        {Object.entries(co2savings).map((value, index) => (
          <Grid item key={index}>
            <Box component="span" sx={{ textTransform: 'capitalize' }}>
              <Typography variant="h6" component="div" align="center" color="text.secondary" gutterBottom>
                {value[0]}
              </Typography>
              <Typography variant="h3" component="div" align="center" color="primary.main" gutterBottom>
                {value[1]}
              </Typography>
            </Box>
          </Grid>
          ))}
      </Grid>
    </Box>
  )
}

function Slide(props) {
  const [error, setError] = React.useState(null);
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [sliderStep, setStep] = React.useState(0);

  const handleChange = (event, newStep) => {
    if (newStep === sliderStep) {return}
    setStep(newStep)

    fetchSavings(newStep)
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          props.setCo2savings(result)
        },
        (error) => {
          console.log({error})
          setIsLoaded(true);
          setError(error);
        })
  }

  return (
    <Slider
      aria-label="Installations"
      defaultValue={DEFAULT_INSTALLS}
      valueLabelDisplay="on"
      step={1}
      marks
      min={1}
      max={10}
      onChangeCommitted={handleChange}
      sx={{
        marginTop: 1
      }}
    />
  );
}

function App() {
  const [error, setError] = React.useState(null);
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [co2savings, setCo2savings] = React.useState({});
  
  React.useEffect(() => {
    fetchSavings(DEFAULT_INSTALLS)
    .then(res => res.json())
    .then(
      (result) => {
        setIsLoaded(true);
        setCo2savings(result)
      },
      (error) => {
        console.log({error})
        setIsLoaded(true);
        setError(error);
      })
  }, []);

  return (
    <Container maxWidth="sm">
      <Box sx={{ my: 4 }}>
        <Typography variant="h4" component="h1" align="center" gutterBottom>
          Termo CO2 savings calculator
        </Typography>
        <ProTip />
        <Slide setCo2savings={setCo2savings}/>
        <Savings co2savings={co2savings}/>
        <Divider />
        <Copyright />
      </Box>
    </Container>
  );
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
    <CssBaseline />
    <App />
  </ThemeProvider>,
  document.querySelector('#app'),
);